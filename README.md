# Select Readings of the Fathers of the Church for Men Discerning Holy Matrimony
## Name
Selected Readings of the Fathers of the Church

## Description
Selected Readings is edited, corrected, and compiled by Augustine Watson out of love for the Most Holy Trinity, thanksgiving for His grace, obedience to the Holy Orthodox Fathers, and fraternal affection for his best friend.

It is to provide an introduction to the Catholic Orthodox Faith for a man approaching the Sacrament of Matrimony.

It contains the texts: The Proto-Evangelium of St. James, The Didache, St. Augustine's 'On the Goods of Marriage', St. John Damascene's 'Sermons on the Dormition of the Virgin', his 'On the Orthodox Faith', and the Canons of Trullo.

The translation of 'On the Orthodox Faith' is corrected to conform with the Greek where the original translator erred.

## Contributing
Please feel free to contribute. The Canons are incompletely formatted, and I'm sure much great work could be done elsewhere.

## License
The font [Acorn Initials](https://www.1001fonts.com/acorn-initials-font.html) is created by Typographer Mediengestaltung. It is used and provided here in conformity with [its license](https://www.1001fonts.com/licenses/ffc.html).


The font [EB Garamond](http://www.georgduffner.at/ebgaramond/) is created by Georg Duffner. It is used and provided here in conformity with [its license](https://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL).


The font [Zallman Caps](https://www.1001fonts.com/zallman-caps-font.html) is created by Typographer Mediengestaltung and D. Rakowski. It is used and provided here in conformity with [its license](https://www.1001fonts.com/licenses/ffc.html).


To the extent possible under law, [Apologia Anglicana](https://ApologiaAnglicana.org) has waived all copyright and related or neighboring rights to 'Selected Readings of the Fathers of the Church', according to the [Creative Commons CC0 1.0 Universal (CC0 1.0) Public Domain Dedication license](https://creativecommons.org/publicdomain/zero/1.0/). This work is published from: United States.
